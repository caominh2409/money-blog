"""init

Revision ID: 92c458ffe793
Revises: 58ac8391f8c2
Create Date: 2022-08-08 11:15:25.170867

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '92c458ffe793'
down_revision = '58ac8391f8c2'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('category', sa.Column('category_code', sa.String(), nullable=True))
    op.add_column('users', sa.Column('role', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'role')
    op.drop_column('category', 'category_code')
    # ### end Alembic commands ###

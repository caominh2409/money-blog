from applications import create_app
from manager import Manager
from applications.models import Users, Category
from applications.database import db

manager = Manager()

def init_app():
    from applications.config import Config
    from applications.server import app
    from applications.database import init_database

    app.config.from_object(Config)
    init_database(app)


@manager.command
def run():
    app = create_app()
    # app.run(host="0.0.0.0", port=15001, debug=True, ssl_context='adhoc')
    app.run(host="0.0.0.0", port=15001, debug=True)

@manager.command
def create_category():
    from applications.config import Config
    from applications.server import app
    from applications.database import init_database

    app.config.from_object(Config)
    init_database(app)
    with app.app_context():
        category = Category(category_name = "Đầu tư", category_code="dau-tu")
        db.session.add(category)
        db.session.commit()
        category = Category(category_name = "Tiết kiệm", category_code="tiet-kiem")
        db.session.add(category)
        db.session.commit()
        category = Category(category_name = "Kiếm tiền", category_code="kiem-tien")
        db.session.add(category)
        db.session.commit()
        category = Category(category_name = "Chứng khoán", category_code="chung-khoan")
        db.session.add(category)
        db.session.commit()
        category = Category(category_name = "Sống", category_code="song")
        db.session.add(category)
        db.session.commit()
        category = Category(category_name = "Thị trường", category_code="thi-truong")
        db.session.add(category)
        db.session.commit()
        category = Category(category_name = "Khác", category_code="khac")
        db.session.add(category)
        db.session.commit()


if __name__ == '__main__':
    manager.main()
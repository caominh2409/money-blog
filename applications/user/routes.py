import json
import re
from flask import Blueprint, redirect, render_template, flash, request, jsonify, Response, url_for
from flask_login import login_user, logout_user, current_user, login_required
from applications.user.forms import LoginForm, RegisterForm
from applications.database import db
from applications.models import Users, Post
from applications.extension import bcrypt

users = Blueprint('user', __name__)

def verify_email(email):
    regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
    if(re.fullmatch(regex, email.strip())):
        return True
    return False

def verify_phone(phone):
    # regex = re.compile('84|0[3|5|7|8|9])+([0-9]{8})\b')
    if phone.isdigit() and len(phone)>=8 and len(phone)<=12 and phone.startswith("0"):
        return True
    return False

@users.route('/login', methods=["GET", "POST"])
def login():
    print('current_user----------', current_user.is_authenticated)
    if current_user.is_authenticated:
        return render_template( url_for('main.home'))

    if request.method == "GET":
        return render_template('login.html')
    elif request.method == "POST":
        param = request.json
        username = param.get("username")
        password = param.get("password")
        check_user = db.session.query(Users).filter(Users.username == username).first()
        if check_user and bcrypt.check_password_hash(check_user.password, password):
            login_user(check_user)
            print('current_user:', current_user)
            return jsonify({}), 200
        return jsonify({"error_message": "Mật khẩu hoặc tên đăng nhập chưa chính xác"}), 520
    return jsonify({"error_message": "Hệ thống đang xảy ra lỗi, vui lòng thư lại sau"}), 520



@users.route('/register', methods=["GET", "POST"])
def register():
    if request.method == "GET":
        return render_template('register.html')
    elif request.method == "POST":
        param = request.json
        username = param.get("username")
        email = param.get("email")
        password = param.get("password")

        if not verify_email(email):
            return jsonify({"error_message": "Email không hợp lệ"}), 520
        if len(username) < 6:
            return jsonify({"error_message": "Tên đăng nhập phải có độ dài từ 6 ký tự trở lên"}), 520
        if len(password) < 6:
            return jsonify({"error_message": "Mật khẩu phải có độ dài từ 6 ký tự trở lên"}), 520

        check_user = db.session.query(Users).filter(Users.username == username).first()
        if check_user:
            return jsonify({"error_message": "Tên đăng nhập đã tồn tại"}), 520
        check_email = db.session.query(Users).filter(Users.email == email).first()
        if check_email:
            return jsonify({"error_message": "Email đã tồn tại"}), 520
        password_user = bcrypt.generate_password_hash(password).decode('utf-8')

        users = Users()
        users.username = username
        users.email = email
        users.password = password_user
        db.session.add(users)
        db.session.commit()
        return jsonify({}), 200
    return jsonify({"error_message": "Hệ thống xảy ra lỗi"}), 520
    
@users.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.home'))


@users.route('/change_like_status', methods=["POST"])
def change_like_status():
    post_id = int(request.json.get("post_id"))
    if not current_user.is_authenticated:
        return jsonify({"error_message": "Vui lòng đăng nhập để thích bài viết"}), 520
    if post_id:
        post = db.session.query(Post).filter(Post.id == post_id).first()
        if post_id in current_user.post_like():
            current_user.liked.remove(post)
            post.liked -= 1
            status = 'unlike'
        else:
            current_user.liked.append(post)
            post.liked += 1
            status = 'like'
        db.session.commit()
        return jsonify({"post_like": post.liked, "status": status}), 200
    return jsonify({"error_message": "Dữ liệu truyền vào thiếu hoặc không chính xác"}), 520
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def init_database(app):
    # db.create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    db.init_app(app)
class Config(object):
    SQLALCHEMY_DATABASE_URI = 'postgresql://bloguser:123456@localhost:5432/moneyblogdb'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_POST_PICTURE = '/static/img/post'
    PREFERRED_URL_SCHEME = 'https'
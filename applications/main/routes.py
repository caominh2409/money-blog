from flask import Blueprint, jsonify, render_template
from datetime import datetime, timedelta
from applications.database import db
from applications.models import Category, Post
main = Blueprint('main', __name__)


def get_top_view():
    end_date = datetime.now()
    start_date = datetime(end_date.year, end_date.month, 1, 0, 0, 0)
    top_post = []
    for post in db.session.query(Post)\
            .filter(Post.time >= start_date.timestamp(),
                    Post.time <= end_date.timestamp()).order_by(Post.date_posted.desc()).limit(3).all():
        post_object = dict()
        category = post.list_categories[0].__dict__
        post = post.__dict__
        post_object['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y")
        post_object['category'] = category['category_name']
        post_object['title'] = post['title']
        post_object['id'] = post['id']
        top_post.append(post_object)

    return top_post

def home_page_post():
    end_date = datetime.now()
    start_date = end_date - timedelta(days=30)
    top_post = []
    for post in db.session.query(Post)\
            .filter(Post.time >= start_date.timestamp(),
                    Post.time <= end_date.timestamp()).order_by(Post.views.desc(), Post.time.desc()).limit(7).all():
        category = post.list_categories[0].__dict__
        post = post.__dict__
        post['category'] = category
        post['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y")
        top_post.append(post)

    top_news = []
    for post in db.session.query(Post).order_by(Post.time.desc()).limit(6).all():
        category = post.list_categories[0].__dict__
        post = post.__dict__
        post['category'] = category
        post['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y")
        top_news.append(post)

    return top_post, top_news

def category_post():
    category_list = db.session.query(Category).all()
    category_post = []
    for category in category_list:
        category_dict = {}
        list_post = []
        if len(category.posts.all()) == 0:
            continue

        all_post = category.posts.all()
        sort_view = sorted(all_post, key=lambda item: item.time)

        for post in sort_view:
            post = post.__dict__ 
            # post['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y")
            post["summary"] = post["summary"] if len(post["summary"].split(" ")) < 40 else " ".join(post["summary"].split(" ")[:40]) + " ..."
            list_post.append(post)
        category_dict['post'] = list_post
        category_dict['category'] = category.__dict__
        category_post.append(category_dict)
    return category_post

@main.route('/')
@main.route('/home')
def home():
    post = dict()
    post['top_post'], post['top_news'] = home_page_post()
    post['category_post'] = category_post()
    for category in post['category_post']:
        print(len(category['post']))
    return render_template('home.html', post=post)

@main.route('/single')
def single():
    return render_template('single.html')

@main.route('/contact')
def contact():
    return render_template('contact.html')

@main.route('/index')
def index():
    return render_template('index.html')

@main.route('/get_post')
def get_post():
    post = dict()
    post['top_post'] = get_top_view()
    return jsonify(post), 200
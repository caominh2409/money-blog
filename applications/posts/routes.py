from crypt import methods
from math import ceil
import os
import secrets
from socket import CAN_RAW
from time import time
from urllib import response
from flask import Blueprint, jsonify, render_template, request, current_app, abort
from flask_login import current_user, login_required
from applications.models import Post, Category, Users, Comment
from .utils import allowed_file
from werkzeug.utils import secure_filename
from applications.server import app
from applications.database import db
from datetime import datetime

posts = Blueprint("post", __name__)


@posts.route('/add_post', methods=['POST', 'GET'])
@login_required
def add_post():
    if request.method == 'GET':
        post_id = request.args.get("post_id")
        if post_id:
            post = db.session.query(Post).filter(Post.id == post_id).first()
            if post:
                return render_template('post/add_post.html', post=post)
        return render_template('post/add_post.html', post=None)
        
    elif request.method == 'POST':
        post_id = request.form['post_id']
        print("post_id", post_id)
        if post_id:
            post = db.session.query(Post).filter(Post.id == post_id).first()
            if post is None:
                post = Post()
        else:
            post = Post()

        post.title = request.form['title']
        post.summary = request.form['summary']
        post.content = request.form['content']
        post.source = request.form['source']
        post.user_id = current_user.id
        post.list_categories = []
        for category_id in request.form['category'].split(','):
            category = db.session.query(Category).filter(Category.id == category_id).first()
            print(category)
            post.list_categories.append(category)

        if 'files[]' in request.files:
            files = request.files.getlist('files[]')
            for file in files:
                if file and allowed_file(file.filename):
                    random_hex = secrets.token_hex(8)
                    _, f_ext = os.path.splitext(file.filename)
                    file.filename = random_hex + f_ext
                    uploads_dir = os.path.join(current_app.root_path, 'static/img/post', secure_filename(file.filename))
                    file.save(uploads_dir)
                    post.post_picture = file.filename

        db.session.add(post)
        db.session.commit()
        return jsonify({}), 200
    return jsonify({"error_message": "Hệ thống đang xảy ra lỗi, vui lòng liên hệ sau"}), 520

def get_top_view():
    end_date = datetime.now()
    start_date = datetime(end_date.year, end_date.month, 1, 0, 0, 0)
    top_post = []
    for post in db.session.query(Post)\
            .filter(Post.time >= start_date.timestamp(),
                    Post.time <= end_date.timestamp()).order_by(Post.views.desc()).limit(3).all():
        category = post.list_categories[0].__dict__
        post = post.__dict__
        # post['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y")
        post['category'] = category
        top_post.append(post)
    return top_post

def get_top_like():
    end_date = datetime.now()
    start_date = datetime(end_date.year, end_date.month, 1, 0, 0, 0)
    top_post = []
    for post in db.session.query(Post)\
            .filter(Post.time >= start_date.timestamp(),
                    Post.time <= end_date.timestamp()).order_by(Post.liked.desc()).limit(5).all():
        timedelta = end_date - post.date_posted
        category = post.list_categories[0].__dict__
        post = post.__dict__
        # post['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y")
        post['category'] = category
        top_post.append(post)
        if timedelta.days != 0:
            post['time_delta'] = f"{timedelta.days} ngày trước"
        elif timedelta.total_seconds()//3600 > 1:
            post['time_delta'] = f"{int(timedelta.total_seconds()//3600)} giờ trước"
        else:
            post['time_delta'] = f"{int(timedelta.total_seconds()//60)} phút trước"
    return top_post
    
def get_same_category(list_category):
    list_post = []
    result = []
    for category in list_category:
        # category = db.session.query(Category).filter(Category.id == category['id']).first()
        if len(category.posts.all()) >= 6:
            list_post = category.posts.all()[:6]
        else: list_post = category.posts.all()

        for post in list_post:
            post = post.__dict__
            post['category'] = category.category_name
            print(post['time'])
            post['time'] = datetime.fromtimestamp(post['time']).strftime("%d-%m-%Y")
            result.append(post)

        return result

@posts.route('/post/<int:post_id>', methods = ['GET'])
def post(post_id):
    post = db.session.query(Post).filter(Post.id == post_id).first()
    if post:

        # Get top6 same category
        same_category = get_same_category(post.list_categories)

        # Get category
        list_categories = []
        for category in post.list_categories:
            list_categories.append(category.__dict__)     
        post = post.__dict__

        # Get top3 view in day
        post["top_view"] = get_top_view()
        
        post["same_category"] = same_category
        # post['time'] = datetime.fromtimestamp(int(post['time'])).strftime("%d-%m-%Y %H:%M")
        post['list_categories'] = list_categories
        post['user_id'] = Users.query.get(int(post['user_id'])).__dict__
        return render_template('post/single.html', post=post, title=post['title'])
    else:
        abort(404)


def new_post():
    now = datetime.now()
    posts = db.session.query(Post).order_by(Post.time.desc()).limit(5).all()
    list_post = []
    for post in posts:
        timedelta = now - post.date_posted
        print(timedelta)
        list_category = [category.__dict__ for category in post.list_categories]
        post = post.__dict__ 
        post['category'] = list_category
        if timedelta.days != 0:
            post['time_delta'] = f"{timedelta.days} ngày trước"
        elif timedelta.total_seconds()//3600 > 1:
            post['time_delta'] = f"{int(timedelta.total_seconds()//3600)} giờ trước"
        else:
            post['time_delta'] = f"{int(timedelta.total_seconds()//60)} phút trước"
        list_post.append(post)
    return list_post

@posts.route("/category/<category_code>", methods=["GET"])
def category(category_code):
    page = request.args.get("page", 1, type=int)
    category = db.session.query(Category).filter(Category.category_code == category_code).first()
    
    if category is None:
        abort(404)

    all_post = category.posts.all()
    sort_view = sorted(all_post, key=lambda item: item.views)

    category = category.__dict__
    category["post"] = []
    # Slice summary
    for post in all_post[page*5 - 5: page*5]:
        post = post.__dict__
        post['time'] = datetime.fromtimestamp(post['time']).strftime("%d-%m-%Y")
        post["summary"] = post["summary"] if len(post["summary"].split(" ")) < 30 else " ".join(post["summary"].split(" ")[:30]) + " ..."
        category["post"].append(post)

    category["total_pages"] = ceil(len(all_post)/5)
    category["page"] = page
    category["top_view"] = [post.__dict__ for post in sort_view[:3]]
    category["new_post"] = new_post()

    return render_template('post/category.html', post=category)

@posts.route('/add_view', methods=["GET"])
def add_view():
    post_id = request.args.get("post_id")
    if post_id:
        post = db.session.query(Post).filter(Post.id == post_id).first()
        if post:
            post.views += 1
            db.session.commit()
            return jsonify({}), 200

@posts.route('/delete', methods=["GET"])
def delete():
    post_id = request.args.get("post_id")
    if post_id:
        post = db.session.query(Post).filter(Post.id==post_id).first()
        if post and post.user_id == current_user.id:
            db.session.delete(post)
            db.session.commit()
            return jsonify({}), 200
        return jsonify({"error_message": "Không tìm thấy bài viết hoặc bạn không phải tác giả bài viết"}), 520
    return jsonify({"error_message": "Hệ thống đang gặp sự cố, vui lòng thử lại sau"}), 520

@posts.route('/get_category', methods=['GET'])
def get_category():
    post_id = request.args.get("post_id")
    if post_id:
        post = db.session.query(Post).filter(Post.id == post_id).first()
        if post:
            category = post.list_categories
            return jsonify({"category": [item.id for item in category]}), 200
        return jsonify({"error_message": "Không tìm thấy bài viết"}), 200
    return jsonify({"error_message": "Tham số truyền vào sai hoặc thiếu"})

@posts.route('/get_all_posts', methods=["GET"])
def get_all_posts():
    page = request.args.get("page", 1, type=int)
    print(page)

    all_post = db.session.query(Post).order_by(Post.time.desc()).limit(5).offset(page*5-5).all()
    post = dict()
    list_post_render = []
    for post in all_post:
        post = post.__dict__
        post['time'] = datetime.fromtimestamp(post['time']).strftime("%d-%m-%Y")
        post["summary"] = post["summary"] if len(post["summary"].split(" ")) < 30 else " ".join(post["summary"].split(" ")[:30]) + " ..."
        list_post_render.append(post)

    print(list_post_render)
    post['render'] = list_post_render
    post['top_view'] = get_top_view()
    post['top_like'] = get_top_like()
    post['page'] = page
    post['total_pages'] = ceil(db.session.query(Post).count() / 5)

    return render_template('post/post.html', post=post)


@posts.route('/post_comment', methods=['POST'])
def post_comment():
    param = request.json
    name = param.get("name", None)
    email = param.get("email", None)
    comment = param.get("comment", None)
    post_id = param.get("post_id")
    reply_comment = int(param.get("reply_comment", 0))

    new_comment = Comment(name=name if name else email, email=email, comment=comment, post_id=post_id)
    if reply_comment:
        new_comment.reply_comment = reply_comment

    db.session.add(new_comment)
    db.session.commit()
    return jsonify({}), 200

@posts.route('/get_comment_data', methods=['GET'])
def get_comment_data():
    param = request.args
    page = int(param.get("page", 1))
    post_id = param.get("post_id", None)
    print(post_id)
    if post_id is None:
        return jsonify({}), 520

    post_comment = db.session.query(Comment).filter(Comment.post_id == post_id, Comment.reply_comment==None)\
        .order_by(Comment.time_comment.desc()).limit(2).offset(page*2-2).all()

    response_post_comment = []
    for comment in post_comment:
        comment = comment.__dict__
        comment.pop("_sa_instance_state")
        comment['time_comment'] = datetime.fromtimestamp(comment['time_comment']).strftime("%d-%m-%Y")

        # Handle reply comment
        all_reply_comment = db.session.query(Comment).filter(Comment.reply_comment==comment['id']).all()
        reply_comment = []
        for reply in all_reply_comment:
            reply = reply.__dict__
            reply.pop("_sa_instance_state")
            reply['time_comment'] = datetime.fromtimestamp(reply['time_comment']).strftime("%d-%m-%Y")
            reply_comment.append(reply)

        comment['reply_comment'] =  reply_comment
        response_post_comment.append(comment)

    return jsonify({"post_comment": response_post_comment}), 200

@posts.route('/get_total_comment', methods=['GET'])
def get_total_comment():
    post_id = request.args.get("post_id", None)
    if post_id is None:
        return jsonify({}), 520

    total_comment = db.session.query(Comment).filter(Comment.post_id == post_id, Comment.reply_comment==None).count()
    return jsonify({"object": total_comment}), 200
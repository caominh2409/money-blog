from flask import Flask, blueprints, redirect, request
from applications.config import Config

app = Flask(__name__)
app.config.from_object(Config)
app.config['SECRET_KEY'] = '123456'
print(app.config['PREFERRED_URL_SCHEME'])

from applications.database import init_database
from applications.extension import init_extension

init_database(app)
init_extension(app)
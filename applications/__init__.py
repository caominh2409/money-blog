from flask import Flask, blueprints
from flask_sqlalchemy import SQLAlchemy
from applications.config import Config
from .server import app

def create_app():
    from applications.main.routes import main
    from applications.user.routes import users
    from applications.posts.routes import posts
    
    app.register_blueprint(main)
    app.register_blueprint(users)
    app.register_blueprint(posts)

    return app
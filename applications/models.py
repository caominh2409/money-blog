from flask_login import UserMixin
from applications.database import db
from datetime import datetime
from applications.extension import login_manager
import time
from math import floor

@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))


posts_categories = db.Table('posts_categories',
                            db.Column('post_id', db.Integer(), db.ForeignKey('post.id', ondelete='cascade'), primary_key=True),
                            db.Column('category_id', db.Integer(), db.ForeignKey('category.id', ondelete='cascade'), primary_key=True))

like_post = db.Table('like_post',
                            db.Column('user_id', db.Integer(), db.ForeignKey('users.id', ondelete='cascade'), primary_key=True),
                            db.Column('post_id', db.Integer(), db.ForeignKey('post.id', ondelete='cascade'), primary_key=True))


class Users(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer(), unique=True, primary_key=True, autoincrement=True)
    username = db.Column(db.String(32), nullable=False)
    password = db.Column(db.String(60), nullable=False)
    avatar = db.Column(db.String(60), default='static/img/default.png')
    email = db.Column(db.String(60))
    is_active = db.Column(db.Boolean, default=1)
    posts = db.relationship('Post', backref='author', lazy=True)
    role = db.Column(db.String(), default="user")
    liked = db.relationship('Post', secondary=like_post, backref='liked_user', lazy=True)

    def __repr__(self):
        return f'{self.username} - {self.email}'

    def post_like(self):
        return [post.id for post in self.liked]

class Post(db.Model):
    __tablename__ = 'post'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    title = db.Column(db.String(), nullable=False)
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), nullable=False)
    views = db.Column(db.Integer(), default = 0)
    summary = db.Column(db.String(), nullable=False)
    liked = db.Column(db.Integer(), default = 0)
    comments = db.relationship('Comment', backref='post', lazy=True)
    post_picture = db.Column(db.String(), default='static/img/post/defaul.png')
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.now())
    list_categories = db.relationship('Category', secondary=posts_categories, backref=db.backref('posts', lazy='dynamic'), overlaps="list_categories,posts")
    time = db.Column(db.BigInteger(), default=floor(time.time()))
    source = db.Column(db.String())

    def __repr__(self):
        return f'Post {self.id} - {self.title}'

class Category(db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer(), index=True, primary_key=True, autoincrement=True)
    category_name = db.Column(db.String(), nullable= False)
    category_code = db.Column(db.String())
    # list_posts = db.relationship('Post', secondary=posts_categories, backref=db.backref('categories', lazy='dynamic'), overlaps="list_categories,posts")

    def __repr__(self):
        return self.category_name


class Comment(db.Model):
    __tablename__ = 'comment'
    id = db.Column(db.Integer(), index=True, primary_key=True, autoincrement=True)
    name = db.Column(db.String())
    email = db.Column(db.String())
    time_comment = db.Column(db.BigInteger(), default=floor(time.time()))
    comment = db.Column(db.String())
    post_id = db.Column(db.Integer(), db.ForeignKey('post.id'), nullable=False)
    reply_comment = db.Column(db.Integer())
(function ($) {
  "use strict";
  $('.btn-form').click(function () {
    var self = this
    var username = $('#username').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var confirm_password = $('#confirm_password').val();
    if (!username || !email || !password || !confirm_password) {
      toast({
        message: "Vui lòng điền đầy đủ thông tin",
        type: "error"
      });
    } else if (password !== confirm_password) {
      toast({
        message: "Xác nhận mật khẩu không chính xác",
        type: "error",
      });
    } else {
      var url = window.location.origin + '/register';
      var data = {
        username: username,
        email: email,
        password: password
      }
      $.ajax({
        type: "POST",
        data: JSON.stringify(data),
        url: url,
        dataType: "json",
        contentType: "application/json",
        success: function (response) {
          toast({message: "Đăng ký tài khoản thành công", type: "success"})
          setTimeout(() => {
            window.location.href = '/login';
          }, 1000);
        },
        error: function (xhr) {
          toast({message: xhr.responseJSON.error_message, type: "error"})
        }
      })
    };
  });
})(jQuery);
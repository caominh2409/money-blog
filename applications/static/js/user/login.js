(function ($) {
    "use strict";
    
    $(".btn-form").click(function() {
        var username = $("#username").val();
        var password = $('#password').val();
    
        if (!username || !password) {
            toast({message: "Không được để trống tên đăng nhập hoặc mật khẩu", type: "error"});
        } else {
            var data = {
                username: username,
                password: password
            }
            var url = window.location.href
            $.ajax({
                url: url,
                type: "POST",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json",
                success: function(response) {
                    toast({message: "Đăng nhập thành công", type: "success"})
                    setTimeout(() => {
                        window.location.href = '/home'
                    }, 1000);
                }
            })
        }
    })
})(jQuery);
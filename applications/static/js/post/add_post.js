(function ($) {
    "use strict";

    var post_id = window.location.search.split("=").at(-1);
    if (post_id) {
        $.ajax({
            url: window.location.origin + "/get_category",
            type: "GET",
            data: {
                post_id: post_id
            },
            dataType: "json",
            contentType: "application/json",
            success: function(response) {
                var items = response.category
                $(document).ready(function(){
                    $('#category').selectize({
                        items : items
                    });
                });
            
            },
            error: function(xhr) {
                toast({message: xhr.responseJSON.error_message, type:"error"})
            }
        })
    } else {
        $(document).ready(function(){
            $('#category').selectize();
        });
    }

    $(".contact100-form-btn").click(function() {
        console.log("an vao roi")
        var form_data = new FormData();
        var ins = document.getElementById('picture').files.length;
        
        for (var index=0; index < ins; index++) {
            form_data.append("files[]", document.getElementById('picture').files[index]);
        }

        form_data.append("category", $('#category').val());
        form_data.append("title", $('#title').val())
        form_data.append("summary", $('#summary').val())
        form_data.append("content", $('#content').val())
        form_data.append("source", $('#source').val())
        form_data.append("post_id", post_id)

        var url = window.location.origin + '/add_post'
        $.ajax({
            url: url,
            type: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                toast({message: "Bài viết của bạn sẽ được đăng ngay sau khi admin duyệt", type: "success"})
                setTimeout(() => {
                    window.location.href = '/home'
                }, 1000);
            },
            error: function(xhr) {
                toast({message: xhr.responseJSON.error_message, type: "error"})
            }
        })
    })
})(jQuery);
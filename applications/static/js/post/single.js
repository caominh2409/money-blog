(function ($) {
    "use strict";

    var post_id = window.location.pathname.split("/").at(-1)

    $('#reply').hide()
    // Set thoi gian dem view
    setTimeout(() => {
        var url = window.location.origin + '/add_view';
        console.log(url)
        $.ajax({
            url: url,
            type: "GET",
            data: {
                post_id: window.location.pathname.split("/").at(-1)
            },
            dataType: "json",
            contentType: "application/json",
            success: function(response) {
                console.log(response)
            },
            error: function(xhr){
                console.log(xhr)
            }
        })
    }, 30000);

    function get_comment_post(post_id, page=1) {
        var data = {
            post_id: post_id,
            page: page
        };
        var url = window.location.origin + "/get_comment_data";
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            contentType: "application/json",
            success: function(response) {
                console.log(response)
                $('#comment').empty()
                response.post_comment.forEach(comment => {
                    var reply_element = ``
                    if (comment.reply_comment !== undefined || len(comment.reply_comment) !== 0) {
                        comment.reply_comment.forEach(comment => {
                            var el = `
                            <div class="media mt-4">
                                <img src="${window.location.origin + "/static/img/default.png"}" alt="Image" class="img-fluid mr-3 mt-1"
                                    style="width: 45px;">
                                <div class="media-body">
                                    <h6><a class="text-secondary font-weight-bold" href="">${comment.name}</a> <small><i>${comment.time_comment}</i></small></h6>
                                    <p>${comment.comment}</p>
                                </div>
                            </div>`
                            reply_element += el
                        });
                    }
                    var new_element = `
                    <div class="media mb-4">
                        <img src="${window.location.origin + "/static/img/default.png"}" alt="Image" class="img-fluid mr-3 mt-1" style="width: 45px;">
                        <div class="media-body">
                            <h6><a class="text-secondary font-weight-bold" href="">${comment.name}</a> <small><i>${comment.time_comment}</i></small></h6>
                            <p>${comment.comment}</p>
                            <button class="btn btn-sm btn-outline-secondary btn-reply" name="${comment.name}" id="${comment.id}">Trả lời</button>
                            ${reply_element}
                        </div>
                    </div>
                    `
                    $('#comment').append(new_element)

                    $('.btn-reply').click(function() {
                        $('#reply').show()
                        $('#reply_comment').hide()
                        $('#reply_comment').val(this.id);
                        $('#comment_content').html(`Trả lời bình luận của ${this.name}`);
                    })
                });
            },
            error: function(xhr) {
                toast({message: "Lấy dữ liệu bình luận bị lỗi", type:"error"})
            }
        })
    };

    function get_total_comment(post_id) {
        var data = {
            post_id: post_id,
        };
        var url = window.location.origin + "/get_total_comment";
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            contentType: "application/json",
            success: function(response) {
                $('#total_comment').html(`${response.object} bình luận`);
                var totalPageComment = Math.ceil(response.object / 2);
                if (totalPageComment !== 0) {
                    $('#pagination').twbsPagination({
                        totalPages: totalPageComment,
                        visiblePages: 5,
                        next: '>>',
                        prev: '<<',
                        first: "",
                        last: "",
                        onPageClick: function (event, page) {
                            get_comment_post(post_id, page=page)
                        }
                    });
                }
            },
            error: function(xhr) {
                toast({message: "Lấy dữ liệu không thành công", type:  "error"})
            }
        })
    };

    get_total_comment(post_id)

    // Handle reply comment
    

    // Post comment
    $('#post_comment').click(function() {
        if ($('#name').val() == null && $('#email').val() == null) {
            toast({message: "Vui lòng nhập tên hoặc email để bình luận", type: "error"})
            return
        };

        if ($('#message').val() == null) {
            toast({message: "Vui lòng nhập bình luận", type: "error"})
            return
        }

        var data = {
            name: $('#name').val(),
            email: $('#email').val(),
            comment: $('#message').val(),
            reply_comment: $('#reply_comment').val(),
            post_id: post_id
        };
        var url = window.location.origin + "/post_comment";
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json",
            success: function(response) {
                toast({message: "Bình luận của bạn đã được đăng", type: "success"})
                var page = document.querySelector("li.page-item.active").firstChild.innerHTML
                get_comment_post(post_id, page=page)
                get_total_comment(post_id)
                $('#name').val(null);
                $('#email').val(null);
                $('#message').val(null);
                $('#reply_comment').val(0)
                $('#reply').hide()
            },
            error: function(xhr) {
                toast({message: xhr.responseJSON.error_message, type: "error"})
            }
        })
    })

    // delete post
    $("#delete").click(function() {
        var url = window.location.origin + '/delete';
        console.log(url)
        $.ajax({
            url: url,
            type: "GET",
            data: {
                post_id: window.location.pathname.split("/").at(-1)
            },
            dataType: "json",
            contentType: "application/json",
            success: function(response) {
                toast({message: "Xóa bài viết thành công", type:"success"});
                setTimeout(() => {
                    window.location.href = "/"
                }, 1000);
            },
            error: function(xhr){
                toast({message: "Đã có lỗi xảy ra, vui lòng thử lại sau", type:"error"})
            }
        })
    })

    // redirect when update post
    $("#update").click(function() {
        window.location.href = '/add_post?post_id=' + window.location.pathname.split("/").at(-1)
    })

    // Like the post
    $("#like").click(function() {
        var url = window.location.origin + "/change_like_status"
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify({
                post_id: post_id
            }),
            contentType: "application/json",
            dataType: "json",
            success: function(response) {
                if (response.status === 'like') {
                    var el = document.getElementById('like');
                    el.classList.remove("far");
                    el.classList.add("fa");
                    el.style.color = 'blue';
                    document.getElementById('post_liked').childNodes[1].nodeValue = response.post_like
                } else {
                    var el = document.getElementById('like');
                    el.classList.remove("fa");
                    el.classList.add("far");
                    el.style.color = null;
                    document.getElementById('post_liked').childNodes[1].nodeValue = response.post_like
                }
            },
            error: function(xhr) {
                toast({message: xhr.responseJSON.error_message, type: "error"})
            }
        })
    })

})(jQuery);
